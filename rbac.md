# RBAC
**RBAC objects determine whether user allowed to perform specific action with regard to type of resource**

_if RBAC does not allow access, access denied by default_

#### Roles: 
Scoped to project namespaces, map allowed actions (verbs) to resource types in namespace

#### ClusterRoles: 
Cluster-wide, map allowed actions (verbs) to cluster-scoped resource types or resource types in any project namespace

#### RoleBindings: 
Grant access by associating Roles or ClusterRoles to users or groups for access within project namespace

#### ClusterRoleBindings: 
Grant access by associating ClusterRoles to users or groups for access to cluster-scoped resources or resources in any project namespace

## Describing Cluster Roles
Use `oc describe` clusterrole to visualize roles in cluster RBAC

- Includes matrix of verbs and resources associated with role

- Lists additional system roles used for OpenShift operations

- For full details use oc get clusterrole -o yaml

## Describing  Roles
Use `oc describe role -n NAMESPACE` to visualize roles in project namespace

- For full details use `oc get role -n NAMESPACE -o yaml`

## Important Cluster Roles

` oc describe clusterrole basic-user `
|  Role |  Description |
|---|---|
| admin  |  Project namespace administrator </br>Rights to manage most resource types in project namespace </br>Can manage RoleBindings within namespace </br>Does not include access to manage ResourceQuotas, LimitRanges, custom resource types | 
basic-user| Can get basic information about projects and users 
 cluster-admin | Can perform any action on any resource type </br> </br>Not intended for use with RoleBindings on namespaces as this permits override of OpenShift security features such as project namespace node restrictions  |
|  edit | Can modify most objects in project </br>Can use oc exec and oc rsh to execute arbitrary commands in containers </br>Cannot view or modify roles or role bindings  |
|  self-provisioner | Can create own projects </br>Automatic administrator of self-provisioned projects </br>Default for all authenticated users  |
| sudoer  | Access to impersonate system:admin user for full access <br> Used with oc --as=system:admin ...  |
| system:image-puller  | Ability to pull container images from image streams in project namespace <br> Used when build and deployment project namespaces separated <br> <br>Used when container images need to be pulled remotely from cluster’s integrated registry  |
| system:image-pusher  | Ability to push container images into image streams in project namespace <br> <br>Used when container images need to be pushed remotely into cluster’s integrated registry  |
|  view |  Can view most objects in project </br> Cannot make any modifications </br> Cannot view or modify roles, role bindings, or secrets|

### Cluster Role Binding Management
- Add cluster role to user:
```
$ oc adm policy add-cluster-role-to-user CLUSTER_ROLE USER
```
- Add cluster role to group:
```
$ oc adm policy add-cluster-role-to-group CLUSTER_ROLE GROUP
```
- Remove cluster role from user:
```
$ oc adm policy remove-cluster-role-from-user CLUSTER_ROLE USER
```
- Remove cluster role from group:
```
 $ oc adm policy remove-cluster-role-from-group CLUSTER_ROLE GROUP
```

## Describing Role Bindings
#### View cluster role bindings
```
oc describe clusterrolebinding cluster-admin cluster-admins
```

#### Addition of Role Bindings in Namespaces
- Add cluster role to user to manage resources in namespace:
```
oc policy add-role-to-user CLUSTER_ROLE USER -n NAMESPACE
```
- Add namespace role to user to manage resources in namespace:
```
oc policy add-role-to-user ROLE USER -n NAMESPACE --role-namespace=NAMESPACE
```

- Add cluster role to group to manage resources in namespace:
```
oc policy add-role-to-group CLUSTER_ROLE GROUP -n NAMESPACE
```

- Add namespace role to group to manage resources in namespace:
```
oc policy add-role-to-group ROLE GROUP -n NAMESPACE --role-namespace=NAMESPACE
```
- Create role bindings using `oc apply`, `oc create` or `modify` to add subjects using `oc apply`, `oc patch`, `oc replace`

_When using `--role-namespace=NAMESPACE` the namespace must match the project namespace, `-n NAMESPACE`._

### Removal of User Role Bindings from Namespaces
- Remove cluster role from user in namespace:
```
$ oc policy remove-role-from-user CLUSTER_ROLE USER -n NAMESPACE
```
- Remove namespace role from user in namespace:
```
$ oc policy remove-role-from-user ROLE USER -n NAMESPACE --role-namespace=NAMESPACE
```
- Remove all role bindings for user in namespace:
```
$ oc policy remove-user USER -n NAMESPACE
```
- Remove role bindings using oc delete or modify to remove subjects using `oc apply`, `oc patch`, `oc replace`

### Removal of Group Role Bindings from Namespaces
- Remove cluster role from group in namespace:
```
$ oc policy remove-role-from-group CLUSTER_ROLE GROUP -n NAMESPACE
```
- Remove namespace role from group in namespace:
```
$ oc policy remove-role-from-group ROLE GROUP -n NAMESPACE --role-namespace=NAMESPACE
```
- Remove all role bindings for group in namespace:
```
$ oc policy remove-group GROUP -n NAMESPACE
```

## Custom Roles
TODO

## Project Self-Provisioning
Normal users create project namespaces through project requests

- oc new-project creates project request (projectrequests.project.openshift.io)

- Project request contains description, display name

- OpenShift creates project namespace for user with annotation openshift.io/requester: USER

- Role binding for admin cluster role created for project requester

- No access for requester to update project namespace after 

**Cluster administrators often disable self-provisioning to more tightly control project management**

### Implementation
self-provisioners cluster role allows users to create project requests

**By default, all OAuth-authenticated users have self-provisioner access:** 

### Disabling Self-Provisioning
1. Set autoupdate annotation to false:
```
$ oc annotate clusterrolebinding self-provisioners rbac.authorization.kubernetes.io/autoupdate=false --overwrite
```
2. Then remove cluster role self-provisioner from system group system:authenticated:oauth:

```
$ oc adm policy remove-cluster-role-from-group self-provisioner system:authenticated:oauth
```

## Troubleshooting

#### Check access to patch namespaces:
```
$ oc auth can-i patch namespaces
```
#### Check access to list pods in openshift-authentication namespace:
```
$ oc auth can-i get pods -n openshift-authentication
```
#### From within OpenShift project, determine which verbs you can perform against all namespace-scoped resources:
```
$ oc policy can-i --list 
```
#### Discover which users can perform action on resource:
```
$ oc policy who-can VERB KIND

$ oc policy who-can get imagestreams -n openshift
```
#### User Impersonation
```
$ oc --as=andrew can-i list pods -n openshift-authentication

$ oc --as=andrew get pods -n example-app-dev
```
#### Group Impersonation
Example: Attempt to get pods in namespace as group:
```
$ oc get pods -n example-app-db \
    --as=: \
    --as-group=example-app-dev
```
Check if project self-provisioning is available for users authenticated with oauth:
```
$ oc auth can-i create projectrequests \
    --as=: \
    --as-group=system:authenticated \
    --as-group=system:authenticated:oauth
```