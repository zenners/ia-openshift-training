# Installing OCP 

## Switch to Root
```
sudo -i
echo ${GUID}
```
## Install the AWS CLI tools:
```
curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
unzip awscli-bundle.zip

# Install the AWS CLI into /bin/aws
./awscli-bundle/install -i /usr/local/aws -b /bin/aws

# Validate that the AWS CLI works
aws --version

# Clean up downloaded files
rm -rf /root/awscli-bundle /root/awscli-bundle.zip
```

## Get the OpenShift installer binary:
```
OCP_VERSION=4.4.3

wget https://mirror.openshift.com/pub/openshift-v4/clients/ocp/${OCP_VERSION}/
openshift-install-linux-${OCP_VERSION}.tar.gz

tar zxvf openshift-install-linux-${OCP_VERSION}.tar.gz -C /usr/bin

rm -f openshift-install-linux-${OCP_VERSION}.tar.gz /usr/bin/README.md

chmod +x /usr/bin/openshift-install
```

## Get the oc CLI tool:

```
wget https://mirror.openshift.com/pub/openshift-v4/clients/ocp/${OCP_VERSION}/openshift-client-linux-${OCP_VERSION}.tar.gz

tar zxvf openshift-client-linux-${OCP_VERSION}.tar.gz -C /usr/bin

rm -f openshift-client-linux-${OCP_VERSION}.tar.gz /usr/bin/README.md

chmod +x /usr/bin/oc
```
#### Check that the OpenShift installer and CLI are in /usr/bin:

```
ls -l /usr/bin/{oc,openshift-install}
```

## Set up bash completion for the OpenShift CLI:


```
oc completion bash >/etc/bash_completion.d/openshift

```

#### Switch out of root with `Ctrl + D`
## Set up AWS Credentials
```
export AWSKEY=<YOURACCESSKEY>
export AWSSECRETKEY=<YOURSECRETKEY>
export REGION=us-east-2

mkdir $HOME/.aws
cat << EOF >>  $HOME/.aws/credentials
[default]
aws_access_key_id = ${AWSKEY}
aws_secret_access_key = ${AWSSECRETKEY}
region = $REGION
EOF
```

Check with

```
aws sts get-caller-identity
```

## Pull Secret
1. Using your browser, go to https://www.openshift.com/try.

2. Click Create your Own Cluster and then Try it in the cloud →


3. Log in with your Red Hat login.

4. For Infrastructure Provider, click AWS.


5. Click Installer-Provisioned-Infrastructure.

6. Ignore all other instructions, find the section Pull Secret, click Copy Pull Secret to place the secret in your clipboard, and then save it to a file.

## Return to your installer VM.

## Create an SSH keypair 

```
ssh-keygen -f ~/.ssh/cluster-${GUID}-key -N ''
```

## Run OpenShift Installer
```
openshift-install create cluster --dir $HOME/cluster-${GUID}

? SSH Public Key /home/<OpenTLC User>/.ssh/cluster-<GUID>-key.pub
? Platform aws
INFO Credentials loaded from the "default" profile in file "/home/<OpenTLC User>/.aws/credentials"
? Region us-east-2 (Ohio)
? Base Domain sandboxNNN.opentlc.com
? Cluster Name cluster-<GUID>
? Pull Secret [? for help] *******************************
```
Sample Output
```
INFO Creating infrastructure resources...
INFO Waiting up to 20m0s for the Kubernetes API at https://api.cluster-b91e.sandbox580.opentlc.com:6443...
INFO API v1.17.1 up
INFO Waiting up to 40m0s for bootstrapping to complete...
INFO Destroying the bootstrap resources...
INFO Waiting up to 30m0s for the cluster at https://api.cluster-b91e.sandbox580.opentlc.com:6443 to initialize...
INFO Waiting up to 10m0s for the openshift-console route to be created...
INFO Install complete!
INFO To access the cluster as the system:admin user when using 'oc', run 'export KUBECONFIG=/home/wkulhane-redhat.com/cluster-b91e/auth/kubeconfig'
INFO Access the OpenShift web-console here: https://console-openshift-console.apps.cluster-b91e.sandbox580.opentlc.com
INFO Login to the console with user: kubeadmin, password: GEveR-tBVTB-jJUJB-iC9Jn
```

## Validate Cluster
```
export KUBECONFIG=$HOME/cluster-${GUID}/auth/kubeconfig

echo "export KUBECONFIG=$HOME/cluster-${GUID}/auth/kubeconfig" >>$HOME/.bashrc

oc whoami
oc get nodes
```