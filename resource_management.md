# Resource Management
- Request = minimum amount of resource a container needs to run

- Limit = maximum amount of resource a container can consume

- Neither is strictly required by scheduler

- Use both whenever possible

## CPU
#### CPU request = minimum amount of CPU required for container to run

- Using less does not affect availability or performance

#### CPU limit = maximum amount of CPU container can consume

- Using more causes system to throttle container

#### Requests and limits assigned based on millicores

- Millicore = unit of CPU measurement, represented as m

- Based on value of 1,000m per CPU

- Example: One half of CPU == 500m

## Memory
#### Memory request = minimum amount of memory required for container to run

- Using less does not affect availability or performance

#### Memory limit = maximum amount of memory container can consume

- Using more causes container to be terminated and (optionally) restarted

#### Memory measured using Mi, Gi, etc.

## Quality of Service (QoS)
- QoS class assigned to pod based on requests and limits
- Aids scheduler in determining pod scheduling
- Class set by Kubernetes based on requirements in table—not configurable

#### BestEffort

- Applied when container requests and limits not defined

#### Burstable

- Applied when container requests defined but limits not defined or higher than the request

#### Guaranteed

- Applied when container requests and limits defined with same value

### CPU QoS
TODO
### Memory QoS 
TOTO

## Limit Ranges
#### Specifies minimum and maximum amount of resources that can be requested by:

- Pods and containers

- Images and image streams

- Persistent volume claims (PVCs)

If new resource violates any enumerated constraints, it is rejected

### Limit Range Management

#### Only cluster admins can create, modify, or delete

- Users can view limit ranges in their projects

#### Set LimitRange objects on every user project (highly recommended)

- Consider project request template or operator

- Ensure that limit ranges created, maintained on all user projects

#### Limit Range Status Example
```
 oc describe limitrange core-resource-limits
```

## Resource Quotas
Builds on requests/limits and limit range functionality

#### Restricts total resource consumption per project or across projects

- Total amount of compute and storage resources consumed in project

- Quantity of specific objects created in project

#### Different from limit ranges

- Does not constrain specifications for individual resources

- Example: Without defined LimitRange, user can request pod with 50Gi of memory if within memory quota!

### Quota Types: Resources

#### CPU and memory

- Sum of CPU and/or memory requests

- Sum of CPU and/or memory limits

#### Storage

- Sum of all storage request sizes and/or PVCs

- Sum of all storage request sizes and/or PVCs for specific storage class
### Quota Types: Objects

- pods

- ReplicationController/ReplicationSet

- services

- configmaps

- services.nodeport

- secrets

- services.loadbalancers

- persistentvolumeclaims

- openshift.io/imagestreams

- resourcequotas

### Quota Management
#### Only cluster admins can create, modify, or delete

- Users can view ResourceQuota in their projects

- Resource quotas need to be set on every user project

- In multi-tenant cluster, resource quotas essential to prevent different teams from negatively affecting others

- Consider project request template or operator

- Ensure resource quotas are created and maintained on all user projects
```
oc describe resourcequotas total-resources
```


