# OCP Common Activities
-  [Authentication](#authentication)
    - [Local Password with HTPassword](#local-password-with-htpassword)
    -  [Disable kubeadmin Account](#disable-kubeadmin-account)
-  [Groups](#group-lab)
    - [Create local-admin Group](#create-local-admin-group)
- [Groups and Cluster RBAC](#groups-and-cluster-rbac)
  - [Configure Direct cluster-admin Access](#configure-direct-cluster-admin-access)
  - [Sudoer cluster-admin Access](#sudoer-cluster-admin-access)
  - [Remove Credentials for system:admin](#remove-credentials-for-system:admin)
- [Project RBAC](#project-rbac)
  - [Build S2I Image](#build-s2i-image)
  - [Attempt Project Deployment](#attempt-project-deployment)
  - [Grant Image Pull Rights](#grant-image-pull-rights)
   


# Authentication
## Login
```
$ cp $HOME/cluster-$GUID/auth/kubeconfig $HOME/.kube/config
```
```
$ oc --user=admin whoami
system:admin
```
## Local Password with HTPassword
```
$ touch htpasswd
```
```
$ htpasswd -Bb htpasswd alice p4ssw0rd
$ htpasswd -Bb htpasswd bob p4ssw0rd
$ htpasswd -Bb htpasswd claire p4ssw0rd
```
```
$ oc --user=admin create secret generic htpasswd \
    --from-file=htpasswd -n openshift-config
```
### Configure OAuth Identity Provider for HTPasswd and Test

Create a file called `$HOME/oauth-config.yaml` with the following contents:
```
apiVersion: config.openshift.io/v1
kind: OAuth
metadata:
  name: cluster
spec:
  identityProviders:
  - name: Local Password
    mappingMethod: claim
    type: HTPasswd
    htpasswd:
      fileData:
        name: htpasswd
```
```
$ oc --user=admin replace -f oauth-config.yaml
oauth.config.openshift.io/cluster replaced
```
```
$ API_URL=$(oc whoami --show-server)
$ oc login -u bob -p p4ssw0rd $API_URL
Login successful.
```
### List Users and Identities
```
$ oc --user=admin get users
NAME    UID                                    FULL NAME   IDENTITIES
alice   899e9805-81c2-11e9-8c05-0a580a80001a               Local Password:alice
bob     a14c7553-81b4-11e9-8c05-0a580a80001a               Local Password:bob
```
```

$ oc --kubeconfig $HOME/cluster-$GUID/auth/kubeconfig get identities
NAME                  IDP NAME        IDP USER NAME  USER NAME  USER UID
```

## LDAP 
TODO
## Disable kubeadmin Account


1. Delete the kubeadmin secret from the kube-system namespace:
```
$ oc --user=admin delete secret kubeadmin -n kube-system
secret "kubeadmin" deleted
```
2. Confirm kubeadmin is not accessible
```
$ API_URL=$(oc whoami --show-server)
$ KUBEADMIN_PASSWORD="$(cat $HOME/cluster-$GUID/auth/kubeadmin-password)"
$ oc login -u kubeadmin -p "$KUBEADMIN_PASSWORD" "$API_URL"

Login failed (401 Unauthorized)
Verify you have provided correct credentials.
```
3. Use TLS authentication to confirm `system:admin` account is still available
```
$ oc --user=admin whoami
system:admin
```
# Groups

## Create `local-admin` Group

1. Create a new group called local-admin:
```
$ oc --user=admin adm groups new local-admin
group.user.openshift.io/local-admin created
```
2. Add the alice user to the local-admin group:
```
$ oc --user=admin adm groups add-users local-admin alice
group.user.openshift.io/local-admin added: "alice"
```
3. Confirm the group configuration:
```
$ oc --user=admin get groups

NAME          USERS
local-admin   alice
```
# Groups and cluster RBAC

## Configure Direct cluster-admin Access
1. Create a cluster role binding to give cluster-admin rights to members of the local-admin group:


```
$ oc --user=admin adm policy add-cluster-role-to-group cluster-admin local-admin

clusterrole.rbac.authorization.k8s.io/cluster-admin added: "local-admin"
```

2. Log in as the alice user to test the administrative access:
```
$ oc login -u alice -p p4ssw0rd

Login successful.
...
```
3. Confirm full administrative access with any verb to any resource type:

```
$ oc auth can-i foo bar
Warning: the server doesn't have a resource type 'bar'
yes
```

## Sudoer cluster-admin Access

1. Create a cluster role binding to grant sudoer rights to members of the ocp-platform group:
```
$ oc adm policy add-cluster-role-to-group sudoer ocp-platform
clusterrole.rbac.authorization.k8s.io/sudoer added: "ocp-platform"
```
2. Log in as a user that belongs to the ocp-platform group and confirm cluster administrative privileges:
```
$ oc login -u david -p r3dh4t1!
Login successful.

You don't have any projects. You can try to create a new project, by running

oc new-project <projectname>
```
3. Test direct cluster administrative access to confirm that it is not available:
```
$ oc auth can-i foo bar
Warning: the server doesn't have a resource type 'bar'
no
```
4. Repeat the same command with the --as=system:admin option using the system:admin account:
```
$ oc --as=system:admin auth can-i foo bar
Warning: the server doesn't have a resource type 'bar'
yes
```

## Remove Credentials for system:admin

Now that you have delegated cluster administrative access, it is no longer appropriate to continue using TLS certificates to access system:admin. In this section, you reset your environment and authenticate as the alice user, to whom you previously delegated cluster-admin access.

1. Capture the cluster API URL from the current .kube/config configuration file:
```
$ API_URL=$(oc whoami --show-server)
``````
2. Remove your kube configuration file:
```
$ rm -f $HOME/.kube/config
```
3. Log in again as the alice user:
```
$ oc login -u alice -p p4ssw0rd $API_URL
The server uses a certificate signed by an unknown authority.
You can bypass the certificate check, but any data you send to the server could be intercepted by others.
Use insecure connections? (y/n): y

Login successful.

... output omitted ...
```

## Restrict Access for Project Self-Provisioning
You remove the user’s default permission to create their own projects and allow only production administrators to create projects. You set a message for users who attempt to create projects without appropriate permissions. Finally, you allow users from the ocp-production group to create their own projects.


1. View the self-provisioners cluster role binding:
```
$ oc get clusterrolebinding self-provisioners -o yaml

apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  annotations:
    rbac.authorization.kubernetes.io/autoupdate: "true"
  creationTimestamp: "2020-05-12T14:00:30Z"
  name: self-provisioners
  resourceVersion: "8347"
  selfLink: /apis/rbac.authorization.k8s.io/v1/clusterrolebindings/self-provisioners
  uid: 52eced3a-5fd0-4d62-87f3-93687701ec6d
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: self-provisioner
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: Group
  name: system:authenticated:oauth
```
- This role binding has the rbac.authorization.kubernetes.io/autoupdate: "true" annotation.

- Typically, you remove a cluster role binding with `oc adm policy remove-cluster-role-from-group` or `oc adm policy remove-cluster-role-from-user`. 
- Because the autoupdate process restores the access, you cannot use either approach with the self-provisioners role binding yet.

2. Set the rbac.authorization.kubernetes.io/autoupdate annotation on the self-provisioners cluster role binding to false:
```
$ oc annotate clusterrolebinding self-provisioners --overwrite \
    rbac.authorization.kubernetes.io/autoupdate=false

clusterrolebinding.rbac.authorization.k8s.io/self-provisioners annotated
```
3. Remove the system:authenticated:oauth group from the self-provisioners cluster role binding:
```
$ oc adm policy remove-cluster-role-from-group \
    self-provisioner system:authenticated:oauth

clusterrole.rbac.authorization.k8s.io/self-provisioner removed: "system:authenticated:oauth"
```

4. Confirm that the self-provisioners cluster role binding still exists and has the rbac.authorization.kubernetes.io/autoupdate: "false" annotation, but no subjects:
```
$ oc get clusterrolebinding self-provisioners -o yaml

apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  annotations:
    rbac.authorization.kubernetes.io/autoupdate: "false"
  creationTimestamp: "2020-05-12T14:00:30Z"
  name: self-provisioners
  resourceVersion: "51729"
  selfLink: /apis/rbac.authorization.k8s.io/v1/clusterrolebindings/self-provisioners
  uid: 52eced3a-5fd0-4d62-87f3-93687701ec6d
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: self-provisioner
```
## Configure Message with Project Request Instructions

With project self-provisioning disabled, it is helpful to provide users with a message to inform them of the correct way to request a new project in OpenShift.

1. Create a file called `$HOME/projects-config.patch.json` with the following JSON patch for the project request message:
```
{
  "spec": {
    "projectRequestMessage": "Please create projects using the portal http://portal.company.internal/provision or PaaS Support at paas-support@example.com"
  }
}
```
2. Patch the cluster resource of kind projects.config.openshift.io with the patch file:
```
$ oc patch projects.config.openshift.io cluster --type=merge \
    -p "$(cat $HOME/projects-config.patch.json)"

project.config.openshift.io/cluster patched
```
3. Log in as the non-admin andrew user and attempt to create a project to verify that the project request message is active:
```
$ oc login -u andrew -p r3dh4t1!
Login successful.

You don't have any projects. Contact your system administrator to request a project.
$ oc new-project test
Error from server (Forbidden): Please create projects using the portal http://portal.company.internal/provision or PaaS Support at paas-support@example.com
```

## Allow Production Administrators to Create Projects

1. Log in as the alice cluster-admin user:
```
$ oc login -u alice -p p4ssw0rd
... output omitted ...
```
2. Use oc adm policy again, but this time add the cluster role of self-provisioner to the ocp-production group:
```
$ oc adm policy add-cluster-role-to-group self-provisioner ocp-production
```
3. Log in to the system as the karla user, a member of the ocp-production group:
```
$ oc login -u karla -p r3dh4t1!
Login successful.

You don't have any projects. You can try to create a new project, by running

    oc new-project <projectname>
```
4. Attempt to create a project as the karla user and verify that this is now successful:
```
$ oc new-project test
Now using project "test" on server "https://api.cluster-7ae9.sandbox134.opentlc.com:6443".

You can add applications to this project with the 'new-app' command. For example, try:

    oc new-app django-psql-example

to build a new example application in Python. Or use kubectl to deploy a simple Kubernetes application:

    kubectl create deployment hello-node --image=gcr.io/hello-minikube-zero-install/hello-node
```
5. Remove the test project:
```
$ oc delete project test
project.project.openshift.io "test" deleted
```

## Restore Self-Provisioner Access to OAuth Authenticated Users

1. Log in as the cluster-admin alice user:
```
$ oc login -u alice -p p4ssw0rd
... output omitted ...
```
2. Restore the rbac.authorization.kubernetes.io/autoupdate: "true" annotation to the self-provisioners cluster role binding:
```
$ oc annotate clusterrolebinding self-provisioners --overwrite \
    rbac.authorization.kubernetes.io/autoupdate=true

clusterrolebinding.rbac.authorization.k8s.io/self-provisioners annotated
```
3. Delete the OpenShift API server pods in the openshift-apiserver namespace to automatically restart them:
```
$ oc delete pod -n openshift-apiserver --all
pod "apiserver-cx5b8" deleted
pod "apiserver-hc458" deleted
pod "apiserver-rqm9n" deleted
```
4. Wait a short time for the API server restart to complete.

5. Verify that the self-provisioners cluster role binding reverted to including the OAuth user group:
```
$ oc get clusterrolebinding self-provisioners -o yaml

apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  annotations:
    rbac.authorization.kubernetes.io/autoupdate: "true"
  creationTimestamp: "2020-05-12T14:00:30Z"
  name: self-provisioners
  resourceVersion: "54094"
  selfLink: /apis/rbac.authorization.k8s.io/v1/clusterrolebindings/self-provisioners
  uid: 52eced3a-5fd0-4d62-87f3-93687701ec6d
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: self-provisioner
subjects:
- apiGroup: rbac.authorization.k8s.io
  kind: Group
  name: system:authenticated:oauth
```

# Project RBAC
## Create and Assign Policies and Permissions
You create projects and policies, and assign them to different groups. You also explore how to grant cluster administration roles.

1. Log in as the alice user:
```
$ oc login -u alice -p p4ssw0rd
Login successful.

... OUTPUT OMITTED ...
```
2. Create the projects for the portalapp and paymentapp applications:
```
$ APPNAME=portalapp
$ APPTEXT="Portal App"

$ oc adm new-project $APPNAME-dev --display-name="$APPTEXT Development"
Created project portalapp-dev

$ oc adm new-project $APPNAME-test --display-name="$APPTEXT Testing"
Created project portalapp-test

$ oc adm new-project $APPNAME-prod --display-name="$APPTEXT Production"
Created project portalapp-prod
```
```
$ APPNAME=paymentapp
$ APPTEXT="Payment App"

$ oc adm new-project $APPNAME-dev --display-name="$APPTEXT Development"
Created project paymentapp-dev

$ oc adm new-project $APPNAME-test --display-name="$APPTEXT Testing"
Created project paymentapp-test

$ oc adm new-project $APPNAME-prod --display-name="$APPTEXT Production"
Created project paymentapp-prod
```
3. Verify that your projects are created:
```
$ oc get projects | grep App
paymentapp-dev                      Payment App Development   Active
paymentapp-prod                     Payment App Production    Active
paymentapp-test                     Payment App Testing       Active
portalapp-dev                       Portal App Development    Active
portalapp-prod                      Portal App Production     Active
portalapp-test                      Portal App Testing        Active
```
4. Assign administrative privileges to the portalapp and paymentapp developer groups for their respective projects—in this case, using the default OpenShift admin cluster role:
```
$ oc policy add-role-to-group admin portalapp -n portalapp-dev
clusterrole.rbac.authorization.k8s.io/admin added: "portalapp"

$ oc policy add-role-to-group admin portalapp -n portalapp-test
clusterrole.rbac.authorization.k8s.io/admin added: "portalapp"

$ oc policy add-role-to-group admin paymentapp -n paymentapp-dev
clusterrole.rbac.authorization.k8s.io/admin added: "paymentapp"

$ oc policy add-role-to-group admin paymentapp -n paymentapp-test
clusterrole.rbac.authorization.k8s.io/admin added: "paymentapp"
```
5. Bind the admin cluster role to the administrators group on the production projects:
```
$ oc policy add-role-to-group admin ocp-production -n portalapp-prod
clusterrole.rbac.authorization.k8s.io/admin added: "ocp-production"

$ oc policy add-role-to-group admin ocp-production -n paymentapp-prod
clusterrole.rbac.authorization.k8s.io/admin added: "ocp-production"
```
## Build S2I Image

1. As the marina user, log in using the command line:
```
$ oc login -u marina -p r3dh4t1!
Login successful.

You have access to the following projects and can switch between them with 'oc project <projectname>':

  * paymentapp-dev
    paymentapp-test

Using project "paymentapp-dev".
```
2. Switch to project paymentapp-dev if it is not already selected:
```
$ oc project paymentapp-dev
Now using project "paymentapp-dev" on server "https://api.cluster-GUID.cluster_domain:6443".
```
3. Build the sinatra example in the paymentapp-dev project:
```
$ oc new-app ruby~https://github.com/openshift/simple-openshift-sinatra-sti \
    --name=sinatra -n paymentapp-dev
     Found image 18a91a0 (10 days old) in image stream "openshift/ruby" under tag "2.5" for "ruby"
... OUTPUT OMITTED ...
--> Creating resources ...
    imagestream.image.openshift.io "sinatra" created
    buildconfig.build.openshift.io "sinatra" created
    deploymentconfig.apps.openshift.io "sinatra" created
    service "sinatra" created
--> Success
    Build scheduled, use 'oc logs -f bc/sinatra' to track its progress.
    Application is not exposed. You can expose services to the outside world by executing one or more of the commands below:
     'oc expose svc/sinatra'
    Run 'oc status' to view your app.
```
4. Wait for the build to complete:
```
$ oc logs -f build/sinatra-1 -n paymentapp-dev
Cloning "https://github.com/openshift/simple-openshift-sinatra-sti" ...
        Commit: d7946efc94139c60d41e974b00c8ea345284819b (Merge pull request #1 from thoraxe/3.0.0-update)
        Author: Erik M Jacobs <erikmjacobs@gmail.com>
        Date:   Fri Jul 3 19:21:33 2015 -0400
Caching blobs under "/var/cache/blobs".
... OUTPUT OMITTED ...
Writing manifest to image destination
Storing signatures

Successfully pushed //image-registry.openshift-image-registry.svc:5000/paymentapp-dev/sinatra:latest@sha256:b6c0159514873b622385a53b7912f1413b41a49b322da1e58a2f0e29759be5ab
Push successful
```
- Note that the image is placed in the paymentapp-dev/sinatra path in the registry corresponding to the namespace and image stream name.

5. When the application has finished building, examine the tags:
```
$ oc describe imagestream sinatra -n paymentapp-dev
Name:                   sinatra
Namespace:              paymentapp-dev
Created:                About a minute ago
Labels:                 app=sinatra
                        app.kubernetes.io/component=sinatra
                        app.kubernetes.io/instance=sinatra
Annotations:            openshift.io/generated-by=OpenShiftNewApp
Image Repository:       image-registry.openshift-image-registry.svc:5000/paymentapp-dev/sinatra
Image Lookup:           local=false
Unique Images:          1
Tags:                   1

latest
  no spec tag

  * image-registry.openshift-image-registry.svc:5000/paymentapp-dev/sinatra@sha256:4cffbbc76a25e69467ac224f43ed4ed6cdacf99ec5720c660adf613528732dc4
      4 seconds ago
```
6. Tag the latest image as test:
```
$ oc tag sinatra:latest sinatra:test -n paymentapp-dev
Tag sinatra:test set to sinatra@sha256:4cffbbc76a25e69467ac224f43ed4ed6cdacf99ec5720c660adf613528732dc4.
```
7. Inspect image stream tags in the paymentapp-dev project namespace:
```
$ oc get imagestreamtags -n paymentapp-dev
NAME             IMAGE REFERENCE                                                                                                                                   UPDATED
sinatra:latest   image-registry.openshift-image-registry.svc:5000/paymentapp-dev/sinatra@sha256:4cffbbc76a25e69467ac224f43ed4ed6cdacf99ec5720c660adf613528732dc4   44 seconds ago
sinatra:test     image-registry.openshift-image-registry.svc:5000/paymentapp-dev/sinatra@sha256:4cffbbc76a25e69467ac224f43ed4ed6cdacf99ec5720c660adf613528732dc4   17 seconds ago
```

## Attempt Project Deployment

In this section, you attempt to deploy the image in the paymentapp-test project. The command is expected to fail due to lack of permissions.

1. Use oc new-app to deploy the paymentapp-dev project’s sinatra image with the test tag:
```
$ oc new-app paymentapp-dev/sinatra:test -n paymentapp-test
--> Found image 0e6a03b (14 minutes old) in image stream "paymentapp-dev/sinatra" under tag "test" for "paymentapp-dev/sinatra:test"
... OUTPUT OMITTED ...
--> Creating resources ...
    imagestreamtag.image.openshift.io "sinatra:test" created
    deploymentconfig.apps.openshift.io "sinatra" created
    service "sinatra" created
--> Success
    Application is not exposed. You can expose services to the outside world by executing one or more of the commands below:
     'oc expose svc/sinatra'
    Run 'oc status' to view your app.
```
2. Note that the deployment fails due to the inability to pull the image:
```
$ oc get pods -n paymentapp-test
NAME               READY   STATUS         RESTARTS   AGE
sinatra-1-2tg8r    0/1     ErrImagePull   0          43s
sinatra-1-deploy   1/1     Running        0          52s
```
- You may find the test deployment succeeds if the pod is scheduled on the node on which the build occurred. This occurs because this node already has the image locally.

## Grant Image Pull Rights

1. Grant image pull rights to the service accounts in the paymentapp-prod and paymentapp-test projects on the paymentapp-dev project:
```
$ oc policy add-role-to-group -n paymentapp-dev \
    system:image-puller system:serviceaccounts:paymentapp-prod
clusterrole.rbac.authorization.k8s.io/system:image-puller added: "system:serviceaccounts:paymentapp-prod"

$ oc policy add-role-to-group -n paymentapp-dev \
    system:image-puller system:serviceaccounts:paymentapp-test
clusterrole.rbac.authorization.k8s.io/system:image-puller added: "system:serviceaccounts:paymentapp-test"
```
2. Assign the registry-viewer role to the ocp-production group so that the production administrators can view the image streams:
```
$ oc policy add-role-to-group registry-viewer ocp-production -n  paymentapp-dev
clusterrole.rbac.authorization.k8s.io/registry-viewer added: "ocp-production"
```
3. As the marina user, restart the build for the sinatra application in the paymentapp-test project:
```
$ oc rollout latest dc/sinatra -n paymentapp-test
deploymentconfig.apps.openshift.io/sinatra rolled out
```
- Note that latest refers to the latest version of the DeploymentConfig. The image tag to be used is still test.

4. After changing the credentials of the service account, verify that the deployment is successful:
```
$ oc get pods -n paymentapp-test
NAME               READY   STATUS      RESTARTS   AGE
sinatra-1-deploy   0/1     Completed   0          11m
sinatra-2-deploy   0/1     Completed   0          101s
sinatra-2-ksscl    1/1     Running     0          92s
```
5. If the deployment is successful, assume that you tested the application and it passed, then tag the image as sinatra:prod and deploy it to the paymentapp-prod project:
```
$ oc tag sinatra:test sinatra:prod -n paymentapp-dev
Tag sinatra:prod set to sinatra@sha256:b6c0159514873b622385a53b7912f1413b41a49b322da1e58a2f0e29759be5ab.
```
6. Again inspect image stream tags in the paymentapp-dev project namespace:

```
$ oc get imagestreamtags -n paymentapp-dev

sinatra:latest   image-registry.openshift-image-registry.svc:5000/paymentapp-dev/sinatra@sha256:4cffbbc76a25e69467ac224f43ed4ed6cdacf99ec5720c660adf613528732dc4   3 minutes ago
sinatra:prod     image-registry.openshift-image-registry.svc:5000/paymentapp-dev/sinatra@sha256:4cffbbc76a25e69467ac224f43ed4ed6cdacf99ec5720c660adf613528732dc4   9 seconds ago
sinatra:test     image-registry.openshift-image-registry.svc:5000/paymentapp-dev/sinatra@sha256:4cffbbc76a25e69467ac224f43ed4ed6cdacf99ec5720c660adf613528732dc4   3 minutes ago
```
7. Log in as the karla user, a member of the ocp-production group, and deploy the prod tag image in the paymentapp-prod project:
```
$ oc login -u karla -p r3dh4t1!
Login successful.

You have access to the following projects and can switch between them with 'oc project <projectname>':

  * paymentapp-dev
    paymentapp-prod
    portalapp-prod

Using project "paymentapp-dev".
```
```

$ oc new-app paymentapp-dev/sinatra:prod -n paymentapp-prod

--> Found image 0e6a03b (32 minutes old) in image stream "paymentapp-dev/sinatra" under tag "prod" for "paymentapp-dev/sinatra:prod"
... OUTPUT OMITTED ...
--> Creating resources ...
    imagestreamtag.image.openshift.io "sinatra:prod" created
    deploymentconfig.apps.openshift.io "sinatra" created
    service "sinatra" created
--> Success
    Application is not exposed. You can expose services to the outside world by executing one or more of the commands below:
     'oc expose svc/sinatra'
    Run 'oc status' to view your app.
```
## LDAP Groups
TODO