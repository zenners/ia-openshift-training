


# OpenShift Container Platform
OpenShift Container Platform is a platform for developing and running containerized applications at scale.

This page contains conceptual information about
-  [Cluster](#cluster)
-  [Nodes](#nodes)
-  [Pods](#pods)
- [Containers](#containers)
- [Images](#images)
- [Builds and ImageStreams](#builds-and-image-streams)
- [Deployments](#deployments)
- [Services](#services)
- [Routes](#routes)
- [Labels](#labels)
## VS Kubernetes
With its foundation in Kubernetes, OpenShift Container Platform incorporates the same technology.
OpenShift Container Platform provides enterprise-ready enhancements to Kubernetes, including the following enhancements:

-   Hybrid cloud deployments. You can deploy OpenShift Container Platform clusters to variety of public cloud platforms or in your data center.
    
-   Integrated Red Hat technology. Major components in OpenShift Container Platform come from Red Hat Enterprise Linux (RHEL) and related Red Hat technologies. OpenShift Container Platform benefits from the intense testing and certification initiatives for Red Hat’s enterprise quality software.
    
-   Open source development model. Development is completed in the open, and the source code is available from public software repositories. This open collaboration fosters rapid innovation and development.
    

Although Kubernetes excels at managing your applications, it does not specify or manage platform-level requirements or deployment processes. Powerful and flexible platform management tools and processes are important benefits that OpenShift Container Platform 4.5 offers. 
# Cluster
**A Kubernetes cluster is a set of node machines for running containerized applications.**

When you deploy programs onto the cluster, it intelligently handles distributing work to the individual nodes for you. If any nodes are added or removed, the cluster will shift around work as necessary. It shouldn’t matter to the program, or the programmer, which individual machines are actually running the code.

At a minimum, a cluster contains a control plane and one or more compute machines, or nodes. The control plane is responsible for maintaining the desired state of the cluster, such as which applications are running and which container images they use.

You can picture it as shown below

![k8 cluster diagram](https://miro.medium.com/max/1400/1*WHXv2Z0bBfC7GW4egoIwTw.png)

# Nodes
Kubernetes runs your workload by placing containers into Pods to run on  _Nodes_. A node may be a virtual or physical machine, depending on the cluster. Each node contains the services necessary to run  [Pods](https://kubernetes.io/docs/concepts/workloads/pods/), managed by the  [control plane](https://kubernetes.io/docs/reference/glossary/?all=true#term-control-plane).

Typically you have several nodes in a cluster. Nodes actually run the applications and workloads.

Every Kubernetes Node runs at least a:

-   **Kubelet**, is responsible for the pod spec and talks to the cri interface
    
-   **Kube proxy**, is the main interface for coms between nodes
    
-   **A container runtime**, (like Docker, rkt) responsible for pulling the container image from a registry, unpacking the container, and running the application.

![](https://thepracticaldev.s3.amazonaws.com/i/rfvpt1fawgau4h35qju6.png)


# Pods
![](https://thepracticaldev.s3.amazonaws.com/i/rf5si3jtu812j9yfw3sa.png)
- OpenShift Online leverages the Kubernetes concept of a _pod_, which is one or more containers deployed together on one host.
- Pods are the smallest compute unit that can be defined, deployed, and managed.
-  Each pod is allocated its own internal IP address, therefore owning its entire port space, and containers within pods can share their local storage and networking.Each pod is allocated its own internal IP address, therefore owning its entire port space, and containers within pods can share their local storage and networking.
- Pods are also treated as expendable, and do not maintain state when recreated. Therefore pods should usually be managed by higher-level controllers rather than directly by users

# Containers
Basic units of OpenShift Online applications are called _containers_.
**Your application is built as a container and packaged inside a pod.**

The relationship between containers, images, and registries is depicted in the following diagram:

![enter image description here](https://i.imgur.com/c2wL4Bl.png)
## Images
Containers in OpenShift Online are based on Docker-formatted container  **_images_**. An image is a binary that includes all of the requirements for running a single container, as well as metadata describing its needs and capabilities.

You can think of it as a packaging technology. Containers only have access to resources defined in the image unless you give the container additional access when creating it. By deploying the same image in multiple containers across multiple hosts and load balancing between them, OpenShift Online can provide redundancy and horizontal scaling for a service packaged into an image.
### Image Registry
Containers in OpenShift Online are based on Docker-formatted container  _images_. An image is a binary that includes all of the requirements for running a single container, as well as metadata describing its needs and capabilities.

You can think of it as a packaging technology. Containers only have access to resources defined in the image unless you give the container additional access when creating it. By deploying the same image in multiple containers across multiple hosts and load balancing between them, OpenShift Online can provide redundancy and horizontal scaling for a service packaged into an image.
# Builds and Image Streams

A _build_ is the process of transforming input parameters into a resulting object. Most often, the process is used to transform input parameters or source code into a runnable **image**. A [`BuildConfig`](https://docs.openshift.com/online/pro/dev_guide/builds/index.html#defining-a-buildconfig) object is the definition of the entire build process.

Build objects share common characteristics:
- inputs for a build
- the need to complete a build process
- logging the build process, 
- publishing resources from successful builds, and 
- publishing the final status of the build. 
- Builds take advantage of resource restrictions, specifying limitations on resources such as CPU usage, memory usage, and build or pod execution time.
The resulting object of a build depends on the builder used to create it. For 

Docker and S2I builds, the resulting objects are **runnable images**. 
For Custom builds, the resulting objects are whatever the builder image author has specified.


## Two ways to build images
#### Source-to-Image (S2I) Build
a tool for building reproducible, Docker-formatted container images. It produces ready-to-run images by injecting application source into a container image and assembling a new image. The new image incorporates the base image (the builder) and built source and is ready to use with the `docker run` command.

![](https://image.slidesharecdn.com/basefarmocpmeetupoffline-170515062105/95/openshift-devops-made-easy-67-638.jpg?cb=1494829454)


#### Pipeline Build
The Pipeline build strategy allows developers to define a _Jenkins pipeline_ for execution by the Jenkins pipeline plugin. The build can be started, monitored, and managed by OpenShift Online in the same way as any other build type.

![jenkinsdiagram](https://www.edureka.co/blog/content/ver.1531719070/uploads/2018/07/Asset-36-1.png)

## Image Streams
The image stream and its tags allow you to see what images are available and ensure that you are using the specific image you need even if the image in the repository changes.

Image streams do not contain actual image data, but present a single virtual view of related images, similar to an image repository.

Using image streams has several significant benefits:

-   You can tag, rollback a tag, and quickly deal with images, without having to re-push using the command line.
    
-   You can trigger Builds and Deployments when a new image is pushed to the registry. Also, OpenShift Online has generic triggers for other resources (such as Kubernetes objects).
    
-   You can  [mark a tag for periodic re-import](https://docs.openshift.com/online/pro/dev_guide/managing_images.html#importing-tag-and-image-metadata). If the source image has changed, that change is picked up and reflected in the image stream, which triggers the Build and/or Deployment flow, depending upon the Build or Deployment configuration.
    
-   You can share images using fine-grained access control and quickly distribute images across your teams.
    
-   If the source image changes, the image stream tag will still point to a known-good version of the image, ensuring that your application will not break unexpectedly.
    
-   You can  [configure security](https://docs.openshift.com/online/pro/admin_guide/manage_rbac.html#admin-guide-manage-rbac)  around who can view and use the images through permissions on the image stream objects.
    
-   Users that lack permission to read or list images on the cluster level can still retrieve the images tagged in a project using image streams.

When using image streams, it is important to understand what the image stream tag is pointing to and how changes to tags and images can affect you. For example:

-   If your image stream tag points to a container image tag, you need to understand how that container image tag is updated. For example, a container image tag  `docker.io/ruby:2.5`  points to a v2.5 ruby image, but a container image tag  `docker.io/ruby:latest`  changes with major versions. So, the container image tag that a image stream tag points to can tell you how stable the image stream tag is.
    
-   If your image stream tag follows another image stream tag instead of pointing directly to a container image tag, it is possible that the image stream tag might be updated to follow a different image stream tag in the future. This change might result in picking up an incompatible version change.

![](https://image.slidesharecdn.com/basefarmocpmeetupoffline-170515062105/95/openshift-devops-made-easy-68-638.jpg?cb=1494829454)


# Deployments
Deployments add expanded support for the software development and deployment lifecycle.

![](https://static.packt-cdn.com/products/9781788997027/graphics/8bc2c7ec-a1de-4eb5-b188-6c05d7e80e26.png)

##  Deployments and DeploymentConfigs
**In the simplest case, a DeploymentConfig creates a new ReplicationController and lets it start up Pods.**

_Deployments_  and  _DeploymentConfigs_  in OpenShift Container Platform are API objects that provide two similar but different methods for fine-grained management over common user applications. They are composed of the following separate API objects:

Terminology Maps as follows:
```mermaid
graph LR
A[Kubernetes] --> B[Deployment] 
B[Deployment]--> C[ReplicationSet]
```
```mermaid
graph LR
A[OCP] --> B[DeploymentConfig] 
B[DeploymentConfig]--> C[ReplicationController]
```
**It is recommended to use Deployments unless you need a specific feature or behavior provided by DeploymentConfigs.**
-   A DeploymentConfig or a Deployment, **either of which describes the desired state** of a particular component of the application as a **Pod template**.
    

    
-   One or more Pods, which represent an instance of a particular version of an application.
### DeploymentConfigs-specific features

##### Automatic rollbacks

- Currently, Deployments do not support automatically rolling back to the last successfully deployed ReplicaSet in case of a failure.

##### Triggers

- Deployments have an implicit  `ConfigChange`  trigger in that every change in the pod template of a deployment automatically triggers a new rollout. If you do not want new rollouts on pod template changes, pause the deployment:
### Deployments-specific features

##### Rollover

- The deployment process for Deployments is driven by a controller loop, in contrast to DeploymentConfigs which use deployer pods for every new rollout. This means that a Deployment can have as many active ReplicaSets as possible, and eventually the deployment controller will scale down all old ReplicaSets and scale up the newest one.

- DeploymentConfigs can have at most one deployer pod running, otherwise multiple deployers end up conflicting while trying to scale up what they think should be the newest ReplicationController. Because of this, only two ReplicationControllers can be active at any point in time. Ultimately, this translates to faster rapid rollouts for Deployments.

##### Proportional scaling

- Because the Deployment controller is the sole source of truth for the sizes of new and old ReplicaSets owned by a Deployment, it is able to scale ongoing rollouts. Additional replicas are distributed proportionally based on the size of each ReplicaSet.

- DeploymentConfigs cannot be scaled when a rollout is ongoing because the DeploymentConfig controller will end up having issues with the deployer process about the size of the new ReplicationController.

##### Pausing mid-rollout

- Deployments can be paused at any point in time, meaning you can also pause ongoing rollouts. On the other hand, you cannot pause deployer pods currently, so if you try to pause a DeploymentConfig in the middle of a rollout, the deployer process will not be affected and will continue until it finishes.

## ReplicationController
A ReplicationController **ensures that a specified number of replicas of a Pod are running at all times.** If Pods exit or are deleted, the ReplicationController acts to instantiate more up to the defined number. Likewise, if there are more running than desired, it deletes as many as necessary to match the defined amount.

A ReplicationController configuration consists of:

-   The **number of replicas** desired (which can be adjusted at runtime).
    
-   A **Pod definition** to use when creating a replicated Pod.
    
-   A **selector for identifying** managed Pods.
    

A selector is a set of labels assigned to the Pods that are managed by the ReplicationController. These **labels** are included in the **Pod** definition that the ReplicationController instantiates. The ReplicationController uses the selector to determine how many instances of the Pod are already running in order to adjust as needed.

The ReplicationController **does not perform auto-scaling based on load or traffic, as it does not track either**. Rather, this requires its replica count to be adjusted by an external auto-scaler.
## ReplicaSets
Similar to a ReplicationController, a ReplicaSet is a native Kubernetes API object that ensures a specified number of pod replicas are running at any given time. The difference between a ReplicaSet and a ReplicationController is that a ReplicaSet supports set-based selector requirements whereas a replication controller only supports equality-based selector requirements.
#### When to use ReplicaSet
Only use ReplicaSets if you require custom update orchestration or do not require updates at all. Otherwise, use Deployments. ReplicaSets can be used independently, but are used by deployments to orchestrate pod creation, deletion, and updates. Deployments manage their ReplicaSets automatically, provide declarative updates to pods, and do not have to manually manage the ReplicaSets that they create.



# Services
### Problem: 
If some set of Pods (call them "backends") provides functionality to other Pods (call them "frontends") inside your cluster, how do the frontends find out and keep track of which IP address to connect to, so that the frontend can use the backend part of the workload?

#### Enter  _Services_

- A   service serves as an internal load balancer. It identifies a set of replicated  pods  in order to proxy the connections it receives to them. 
- Backing pods can be added to or removed from a service arbitrarily while the service remains consistently available, enabling anything that depends on the service to refer to it at a consistent address. The default service clusterIP addresses are from the OpenShift Online internal network and they are used to permit pods to access each other.
- Services are assigned an IP address and port pair that, when accessed, proxy to an appropriate backing pod. A service uses a label selector to find all the containers running that provide a certain network service on a certain port.

![](https://i.stack.imgur.com/Slh0q.png)

# Routes
An OpenShift Online route exposes a  [service](https://docs.openshift.com/online/pro/architecture/core_concepts/pods_and_services.html#services)  at a host name, such as  _www.example.com_, so that external clients can reach it by name.

DNS resolution for a host name is handled separately from routing. Your administrator may have configured a DNS wildcard entry that will resolve to the OpenShift Online node that is running the OpenShift Online router. If you are using a different host name you may need to modify its DNS records independently to resolve to the node that is running the router.

# Labels
Labels are used to organize, group, or select API objects. For example, pods are "tagged" with labels, and then services use label selectors to identify the pods they proxy to. This makes it possible for services to reference groups of pods, even treating pods with potentially different containers as related entities.

Labels are simple key/value pairs, as in the following example:
```
labels:
  key1: value1
  key2: value2
  ```